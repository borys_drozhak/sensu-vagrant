# == Class: baseconfig
#
# Performs initial configuration tasks for all Vagrant boxes.
#
class baseconfig {
  # exec { 'apt-get update':
  #   command => '/usr/bin/apt-get update';
  # }

  host { 'puppet':
    ip => '192.168.0.10';
  }

  package { ['htop', 'tree', 'unzip']:
    ensure => present;
  }

  file {
    '/home/vagrant/.bashrc':
      owner => 'vagrant',
      group => 'vagrant',
      mode  => '0644',
      source => 'puppet:///modules/baseconfig/bashrc';
  }
}