resource "vsphere_virtual_machine" "Sensu-API" {
  name   = "Sensu-API"
  vcpu   = 2
  memory = 4096
  network_interface {
    label = "CiscoStub_inc_243032"
  }
  disk {
    datastore = "ESX46_Storage1"
    size = 20
    template = "Template_CentOS_7"
  }
}

resource "vsphere_virtual_machine" "Sensu-Server" {
  name   = "Sensu-Server"
  vcpu   = 2
  memory = 4096
  network_interface {
    label = "CiscoStub_inc_243032"
  }
  disk {
    datastore = "ESX46_Storage1"
    template = "Template_CentOS_7"
    size = 20
  }
}

resource "vsphere_virtual_machine" "RabbitMQ-Redis" {
  name   = "RabbitMQ-Redis"
  vcpu   = 2
  memory = 4096
  network_interface {
    label = "CiscoStub_inc_243032"
    ip_address = "172.18.18.13"
    subnet_mask = "255.255.255.0"
  }
  disk {
    datastore = "ESX46_Storage1"
    size = 20
    template = "Template_CentOS_7"
  }
}

resource "vsphere_virtual_machine" "Sensu-Client" {
  name   = "Sensu-Client"
  vcpu   = 2
  memory = 4096
  network_interface {
    label = "CiscoStub_inc_243032"
    ip_address = "172.18.18.14"
    subnet_mask = "255.255.255.0"
  }
  disk {
    datastore = "ESX46_Storage1"
    size = 10
    template = "Template_CentOS_7"
  }
}

resource "vsphere_virtual_machine" "Sensu-Client-2" {
  name   = "Sensu-Client-2"
  vcpu   = 2
  memory = 4096
  network_interface {
    label = "CiscoStub_inc_243032"
    ip_address = "172.18.18.15"
    subnet_mask = "255.255.255.0"
  }
  disk {
    datastore = "ESX46_Storage1"
    size = 10
    template = "Template_CentOS_7"
  }
}

output "address" {
  value = "${vsphere_virtual_machine.Sensu-Client.ip_address}"
}