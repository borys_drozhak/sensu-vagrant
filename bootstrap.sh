#!/bin/bash

EL=`uname -r |grep el[0-7] -o`

# epel :
 if rpm -qa|grep epel-rel -q ; then
    echo 'epel-repo is installed'
 else
    if    [ "$EL" = 'el6' ] ; then
        rpm -ivh http://ftp-srv2.kddilabs.jp/Linux/distributions/fedora/epel/6/x86_64/epel-release-6-8.noarch.rpm
    elif  [ "$EL" = 'el7' ]; then
        yum install epel-release.noarch -y
    fi
 fi      

# puppet repo
 if rpm -qa|grep puppetlabs-release -q ; then
    echo 'puppet-repo is installed'
 else    
    if    [ "$EL" = 'el6' ] ; then
        echo 'bootstrap: oops! ADD puppet repo!'
    elif  [ "$EL" = 'el7' ]; then
    rpm -ivh https://yum.puppetlabs.com/puppetlabs-release-el-7.noarch.rpm
    fi    
fi

# installation
if echo $(rpm -qa) |grep puppet|grep json|grep jq|grep gem |grep openssl|grep git -q  ; then
    echo 'Seems all require packages are okay'
else    
    yum -y install puppet-3* rubygem-json jq openssl wget curl git  bind-utils ruby-dev gem
fi

# puppet modules (I'll try use r10k or smthing soon)
if echo $(sudo puppet module list) |grep sensu|grep rabbitmq|grep erlang|grep redis|grep uchiwa| grep stdlib -q ; then
    echo 'modules: sensu, rabbitmq, redis, stdlib, uchiwa, erlang, gem are installed. passing'
else    
    # puppet module install example42/redis
    # puppet module install puppetlabs/rabbitmq
    # puppet module install sensu/sensu
    # puppet module install yelp-uchiwa
    # puppet module install garethr-erlang

# gem install --no-ri --no-rdoc r10k
# cd /etc/puppet
# rm -rf modules/
# LIBRARIAN_FILE=$( cat << EOF
# forge "http://forge.puppetlabs.com"
# mod "arioch/redis", "1.1.3"
# mod "sensu/sensu", "2.0.0"
# mod "puppetlabs/stdlib"
# mod "maestrodev/wget"
# mod "garethr/erlang", "0.3.0"
# mod "puppetlabs/rabbitmq", "5.3.1"
# mod "nanliu/staging"
# mod "yelp/uchiwa", "0.3.0"
# EOF
# )
# echo "${LIBRARIAN_FILE}" > /etc/puppet/Puppetfile
# sudo r10k puppetfile install -v

puppet module install arioch/redis
puppet module install sensu/sensu
puppet module install puppetlabs/stdlib
puppet module install maestrodev/wget
puppet module install garethr/erlang
puppet module install puppetlabs/rabbitmq
puppet module install nanliu/staging
puppet module install yelp/uchiwa

    # warnings sux
    sed -i '/^templatedir/d' /etc/puppet/puppet.conf
fi    

# temporary
touch /etc/puppet/hiera.yaml

## generating uniq keys
if [ ! -f '/etc/ssl/sensu/server/cert.pem' ] ; then

# http://sensuapp.org/docs/0.12/certificates 
echo 'keys genaration ..'

# generate sensu SSL certificates to the puppet manifest can use them
cd /root
if [ ! -f ssl_certs.tar ] ; then
 wget http://sensuapp.org/docs/0.20/tools/ssl_certs.tar
 tar -xvf ssl_certs.tar
fi 
cd ssl_certs
./ssl_certs.sh generate &>/dev/null

# cd /tmp/
# git clone git://github.com/joemiller/joemiller.me-intro-to-sensu.git  # it's temporary, dude
# cd joemiller.me-intro-to-sensu
# ./ssl_certs.sh generate  2>&1 1>/dev/null
# sudo mkdir -p /etc/puppet/files/sensu/
# sudo cp *.pem testca/*.pem /etc/puppet/files/sensu/
# sudo mkdir -p /etc/puppet/modules/sensu/files
# sudo cp *.pem testca/*.pem /etc/puppet/modules/sensu/files/


else 
    echo 'seems the keys are already added'
fi

### workaround for Centos 7 If u kind-hearted and want leave SElinux alone
echo 'try: setsebool -P nis_enabled 1' && ( sudo setsebool -P nis_enabled 1 || echo oops)

# sudo systemctl start firewalld
# sudo firewall-cmd --permanent --add-port=4369/tcp  # Erland and EPMD ports (if cluster)
# sudo firewall-cmd --permanent --add-port=25672/tcp 
# sudo firewall-cmd --permanent --add-port=5672/tcp  # main
# sudo firewall-cmd --permanent --add-port=5671/tcp  # ssl
# sudo firewall-cmd --permanent --add-port=15672/tcp # management for rabbit
# sudo firewall-cmd --permanent --add-port=4567/tcp  # uchiwa
# sudo firewall-cmd --reload

## redis improve:
 sysctl -w vm.overcommit_memory=1
 sysctl -w net.core.somaxconn=512


 ### rabbit-module
 # /etc/puppet/modules/rabbitmq/manifests/install/rabbitmqadmin.pp

 #     tries       => 30,
 #    try_sleep   => 6,