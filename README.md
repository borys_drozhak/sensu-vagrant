# Hey #

### repo for automation provisioning sensu claster via vagrant + puppet 3 at Centos 7 boxes ###


## requirements:
 # vagrant plugin install vagrant-vbguest
 # Guest Additions update for Linux guests
 # rsync(for virtual box provider)



Main body in Vagrant file: 

```
#!ruby

nodes = [
{ :hostname => 'ops-sensu-app1', :ip => '10.170.0.2',    :box => box_7, :ram => 1024 },
{ :hostname => 'client1',        :ip => '10.170.0.11', :box => box_7 },
]
```

Main manifest: => sensu-vagrant / puppet / manifests / site.pp

some Global variables: => sensu-vagrant / hiera / common.yaml